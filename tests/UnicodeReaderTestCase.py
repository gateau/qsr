# vi: expandtab tabstop=4
# encoding: utf8
import os
import sys
import unittest

sys.path.append("../qsr")

from UnicodeReader import UnicodeReader

TEST_TEXT = unicode("éèà", "utf8")

class UnicodeReaderTestCase(unittest.TestCase):
    def test(self):
        encodingList = ['utf8', 'iso8859-15']
        reader = UnicodeReader()
        reader.setEncodingList(encodingList)

        for expectedEncoding in encodingList:
            binaryData = TEST_TEXT.encode(expectedEncoding)

            text, encoding = reader.read(binaryData)

            self.assertEquals(text, TEST_TEXT)
            self.assertEquals(encoding, expectedEncoding)

if __name__ == "__main__":
    unittest.main()
