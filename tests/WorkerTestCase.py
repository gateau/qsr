# vi: expandtab tabstop=4
import os
import shutil
import sys
import re
import unittest

import sandbox

sys.path.append("../qsr")

from Worker import Worker


class WorkerTestCase(unittest.TestCase):
    def setUp(self):
        sandbox.create()

    def tearDown(self):
        shutil.rmtree(sandbox.SANDBOX_DIR)
        pass

    def testSearch(self):
        worker = Worker()
        worker.setDir(sandbox.SANDBOX_DIR)
        worker.setMode(Worker.Search)
        searchRE = re.compile(re.escape(u"Hello World"))
        worker.setSearchRE(searchRE)
        worker.setPatterns(["*.c"])
        worker.start()
        worker.wait()
        matches = worker.matches()
        self.assertEquals(len(matches), 1)

        worker.setPatterns(["*.txt"])
        worker.start()
        worker.wait()
        matches = worker.matches()
        self.assertEquals(len(matches), 0)


    def testReplace(self):
        src = u"Hello World"
        dst = u"Bye bye World"
        worker = Worker()
        worker.setDir(sandbox.SANDBOX_DIR)
        worker.setMode(Worker.Replace)
        worker.setPatterns(["*.c"])
        searchRE = re.compile(re.escape(src))
        worker.setSearchRE(searchRE)
        worker.setDst(dst)
        worker.start()
        worker.wait()
        matches = worker.matches()
        self.assertEquals(len(matches), 1)
        fileName = matches[0][0]
        data = file(fileName).read()
        self.assert_(src not in data)
        self.assert_(dst in data)

if __name__ == "__main__":
    unittest.main()
