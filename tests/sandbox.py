# vi: expandtab tabstop=4
import os
import shutil

SANDBOX_DIR="sandbox"

SANDBOX_FILES={
    "helloworld.c": """
#include <stdio.h>
int main() {
    printf("Hello World\\n");
    return 0;
}
""",

    "helloworld.html": """
<html>
<body>
Hello World
</body>
</html>
"""
    }

def create():
    if os.path.exists(SANDBOX_DIR):
        shutil.rmtree(SANDBOX_DIR)
    os.mkdir(SANDBOX_DIR)
    for fileName, content in SANDBOX_FILES.items():
        fileName = os.path.join(SANDBOX_DIR, fileName)
        dirName = os.path.dirname(fileName)
        if not os.path.exists(dirName):
            os.makedirs(dirName)
        file(fileName, "w").write(content)


if __name__ == "__main__":
    create()
