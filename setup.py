#!/usr/bin/env python
# encoding: utf-8

from kdistutils import setup

import qsr

NAME = "qsr"

DESCRIPTION = "Qt Search and Replace"

setup(name=NAME,
    version=qsr.VERSION,
    description=DESCRIPTION,
    author="Aurélien Gâteau",
    author_email="agateau@kde.org",
    license="GPLv2 or later",
    platforms=["any"],
    requires=["PyQt4"],
    url="http://agateau.com/projects/qsr",
    packages=["qsr"],
    data_files=[
        ("share/applications/", ["desktop/qsr.desktop"]),
        ("share/doc/kidmp", ["LICENSE", "README.md"]),
    ],
    scripts=["bin/qsr"],
    )
