# What is it?

QSR is a recursive search-and-replace utility build with PyQt4.

It makes it possible to perform search or search-and-replace operations on all
files or a subset of files of a directory.

# Most interesting features

Files can be selected with one or more wildcards. For example, enter "*.h *.cpp"
to look for C++ source files.

Search pattern may be either plain-text or a Python regular expression and may
be case-sensitive or not.

Replacement expression can use Python placeholders to reuse parts of the search
pattern. For example if the search pattern is "int (foo|bar|baz);", and
replacement expression is "long \1;" then "int foo;" will be replaced with
"long foo;"

Interactive editing: after a search, it is possible to click on a search result
to edit the text at the matching line from within the application. Double
clicking the result opens the file at the matching line with an external editor.

# Installation

Make sure PyQt4 is installed

Run `python setup.py install`

# Configuration

There is no configuration dialog for now, so one must edit
`~/.config/qsr/qsr.conf` for now.
