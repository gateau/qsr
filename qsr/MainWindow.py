# vi: expandtab tabstop=4
import os
import re
import sys

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from Worker import Worker
from ui_MainWindowBase import Ui_MainWindowBase
from Config import Config
from EditorController import EditorController
from Highlighter import Highlighter

def prepareSearchRE(reText, isRegex, ignoreCase):
    if not isRegex:
        reText = re.escape(reText)

    if ignoreCase:
        flags = re.IGNORECASE
    else:
        flags = 0

    return re.compile(reText, flags)

# A list of "usable" (yes, this is subjective) fixed pitch fonts
PREFERED_FONTS = [
    "Deja Vu Sans Mono",
    "Bitstream Vera Mono",
    "Andale Mono",
    "Monospace",
    "Courier New"
    ]

def findFixedPitchFonts():
    db = QFontDatabase()
    lst = [x for x in db.families() if db.isFixedPitch(x)]
    return lst


class SearchTreeWidgetItem(QTreeWidgetItem):
    def __init__(self, parent, text1, text2):
        QTreeWidgetItem.__init__(self, parent)
        self.setText(0, text1)
        self.setText(1, text2)

    def fileInfo(self):
        return unicode(self.text(0)).split(':')


class SearchItemDelegate(QItemDelegate):
    """
    A delegate which uses Highlighter to highlight matches
    """
    def __init__(self, *args):
        QItemDelegate.__init__(self, *args)
        self._highlighter = Highlighter(self)


    def setHighlightRE(self, highlightRE):
        self._highlighter.setHighlightRE(highlightRE)


    def paint(self, painter, option, index):
        if index.column() == 0:
            QItemDelegate.paint(self, painter, option, index)
            return

        left = option.rect.left()
        top = option.rect.top() - 1
        text = index.data().toString()

        painter.translate(left, top)
        context = QAbstractTextDocumentLayout.PaintContext()
        doc = QTextDocument()
        doc.setPlainText(text)
        self._highlighter.setDocument(doc)
        doc.documentLayout().draw(painter, context)
        painter.translate(-left, -top)

        if doc.documentLayout().documentSize().width() == -1:
            # The code above results in an empty cell with Qt 4.1, so fallback
            # to the normal delegate instead.
            # We can't rely on QT_VERSION to detect Qt 4.1, because QT_VERSION
            # contains the version of the Qt lib used to build the Python
            # bindings, not the version of the installed Qt lib.
            QItemDelegate.paint(self, painter, option, index)


class ReplaceTreeWidgetItem(QTreeWidgetItem):
    def __init__(self, parent, name, count):
        QTreeWidgetItem.__init__(self, parent)
        self._name = name
        self.setText(0, name)
        self.setText(1, str(count))

    def fileName(self):
        return unicode(self._name)


class MainWindow(QWidget, Ui_MainWindowBase):
    def __init__(self):
        QWidget.__init__(self)
        # This list prevents Python from garbage collecting some objects
        self._refs = []
        self._config = Config()
        self.setupUi(self)
        self.initUi()
        self._itemDelegate = SearchItemDelegate()
        self.lvSearchTrace.setItemDelegate(self._itemDelegate)

        self._mode = Worker.Unknown
        self._matchList = []
        self._absoluteDir = ""
        self.btnSearch.setDefault(True)
        self._editorController = EditorController(self.txtContent)

        self._chrono = QTime()
        self._pollTimer = QTimer(self)

        self.txtFilePattern.setText(self._config.patterns)
        
        self.initSignals()

    
    def initUi(self):
        font = QFont(self.txtContent.font())
        lst = findFixedPitchFonts()
        for family in PREFERED_FONTS:
            if family in lst:
                break
        font.setFamily(family)
        self.txtContent.setFont(font)

        # Set titles to bold
        for lbl in self.lblFilesTitle, self.lblSearchingTitle, self.lblReplacingTitle:
            font = QFont(lbl.font())
            font.setBold(True)
            lbl.setFont(font)

        # baseFolder
        if len(sys.argv) > 1:
            baseDir = QString(os.path.abspath(sys.argv[1]))
        else:
            baseDir = self._config.baseDir

        if baseDir.isEmpty():
            baseDir = QString(os.getcwd())

        self.txtBaseFolder.setText(baseDir)

        completer = QCompleter(self.txtBaseFolder)
        dirModel = QDirModel(completer)
        dirModel.setFilter(QDir.Dirs | QDir.NoDotAndDotDot)
        completer.setModel(dirModel)
        self.txtBaseFolder.setCompleter(completer)
        self._refs.append(completer)


    def initSignals(self):
        QObject.connect(self.btnBrowse,SIGNAL("clicked()"),self.selectDir)
        QObject.connect(self.btnSearch,SIGNAL("clicked()"),self.search)
        QObject.connect(self.txtSrc, SIGNAL("returnPressed()"), self.search)
        QObject.connect(self.btnReplace,SIGNAL("clicked()"),self.replace)
        QObject.connect(self.lvSearchTrace,SIGNAL("currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)"), self.openItem)
        QObject.connect(self.lvSearchTrace,SIGNAL("itemDoubleClicked(QTreeWidgetItem*, int)"), self.openItemWithEditor)
        QObject.connect(self.lvReplaceTrace,SIGNAL("itemDoubleClicked(QTreeWidgetItem*, int)"), self.openItemWithEditor)
        QObject.connect(self.tbSave, SIGNAL("clicked()"), self._editorController.save)
        QObject.connect(self.tbPrevious, SIGNAL("clicked()"), self.goToPrevious)
        QObject.connect(self.tbNext, SIGNAL("clicked()"), self.goToNext)
        QObject.connect(self.txtContent.document(), SIGNAL("modificationChanged(bool)"), self.tbSave.setEnabled)
        QObject.connect(self._pollTimer, SIGNAL("timeout()"), self.slotPoll)

    #
    # Common
    #
    def selectDir(self):
        dir = QFileDialog.getExistingDirectory(self, self.tr("Select the base folder"), self.txtBaseFolder.text())
        if not dir.isEmpty():
            self.txtBaseFolder.setText(dir)


    def commonInit(self):
        def showError(msg):
            QMessageBox.warning(self, self.tr("Error"), \
                msg, QMessageBox.Ok, QMessageBox.NoButton)

        self._absoluteDir = os.path.abspath(unicode(self.txtBaseFolder.text()))
        filePatterns = unicode(self.txtFilePattern.text()).strip()
        src = unicode(self.txtSrc.text())
        
        # Check params
        if not os.path.exists(self._absoluteDir):
            showError(self.tr("Folder '%1' does not exist.").arg(self._absoluteDir))
            return False
        
        if filePatterns == "":
            showError(self.tr("No file pattern specified."))
            return False
        
        if src == "":
            showError(self.tr("No search text specified."))
            return False

        try:
            searchRE = prepareSearchRE(src, self.chkRegex.isChecked(), self.chkIgnoreCase.isChecked())
        except re.error, exc:
            showError(self.tr("Invalid regular expression: %1").arg(unicode(exc)))
            return False

        self._editorController.clear()

        # Start init
        self._chrono.start()
        self._pollTimer.start(100)
        self._matchList = []

        self._worker = Worker()
        self._worker.setDir(self._absoluteDir)
        self._worker.setPatterns(filePatterns.split(" "))
        self._worker.setExcludedDirs(unicode(self._config.excludedDirs).split(" "))
        self._worker.setSearchRE(searchRE)

        return True


    def openItemWithEditor(self, item):
        if self._mode == Worker.Unknown or item is None:
            return
            
        if self._mode == Worker.Search:
            name, num = SearchTreeWidgetItem.fileInfo(item)
        else:
            name = ReplaceTreeWidgetItem.fileName(item)
            num = 1

        path = os.path.join(self._absoluteDir, name)
        
        cmdLine = QString(self._config.editor)
        cmdLine = cmdLine.replace("%f", path).replace("%n", str(num))
        QProcess.startDetached(cmdLine)

    
    def closeEvent(self, event):
        event.accept()
        self._config.patterns = self.txtFilePattern.text()
        self._config.baseDir = self.txtBaseFolder.text()
        self._config.save()


    def slotPoll(self):
        if self._worker.isRunning():
            progress = self._worker.progress()
            self._dlg.setValue(progress[0])
            self._dlg.setMaximum(progress[1])
        else:
            self._dlg.close()
            self._pollTimer.stop()
            self._matchList = self._worker.matches()
            if self._mode == Worker.Search:
                self.fillSearchListView()
            else:
                self.fillReplaceListView()


    #
    # Search only
    #
    def openItem(self, item):
        assert self._mode == Worker.Search, "MainWindow.openItem: wrong mode"
        if item is None:
            return
            
        name, num = SearchTreeWidgetItem.fileInfo(item)
        path = os.path.join(self._absoluteDir, name)
        self._editorController.setHighlightRE(self._worker.searchRE())
        self._editorController.open(path, int(num))
        
    def goToPrevious(self):
        item = self.lvSearchTrace.currentItem()
        if not item:
            return

        idx = self.lvSearchTrace.indexOfTopLevelItem(item)
        if idx == 0:
            return

        item = self.lvSearchTrace.topLevelItem(idx - 1)
        self.lvSearchTrace.setCurrentItem(item)
        self.lvSearchTrace.scrollToItem(item)

    def goToNext(self):
        item = self.lvSearchTrace.currentItem()
        if not item:
            return

        idx = self.lvSearchTrace.indexOfTopLevelItem(item)
        if idx == self.lvSearchTrace.topLevelItemCount() - 1:
            return

        item = self.lvSearchTrace.topLevelItem(idx + 1)
        self.lvSearchTrace.setCurrentItem(item)
        self.lvSearchTrace.scrollToItem(item)

    def search(self):
        self._mode = Worker.Search
        if not self.commonInit():
            return
        self._worker.setMode(Worker.Search)
        
        self.stkResult.setCurrentWidget(self.pageSearch)
        self.lvSearchTrace.clear()
        
        self._dlg = QProgressDialog(self)
        self._dlg.setLabelText(self.tr("Searching..."))
        self._dlg.setMinimumDuration(0)
        QObject.connect(self._dlg, SIGNAL("canceled()"), self._worker.requestCancel)
        
        self._worker.start()
        self._dlg.exec_()


    def fillSearchListView(self):
        self._itemDelegate.setHighlightRE(self._worker.searchRE())
        for filename,num,line in self._matchList:
            filename = os.path.abspath(filename)
            filename = filename[len(self._absoluteDir)+1:]

            SearchTreeWidgetItem(
                self.lvSearchTrace,
                filename + ':' + str(num), 
                line
                )

        itemCount = self.lvSearchTrace.topLevelItemCount()
        self.lblStatus.setText(
            self.tr("%1 matches (took %2 ms)")
            .arg(itemCount)
            .arg(self._chrono.elapsed()) )
        if itemCount > 0:
            item = self.lvSearchTrace.topLevelItem(0)
            self.lvSearchTrace.setCurrentItem(item)


    #
    # Replace only
    #
    def replace(self):
        self._mode = Worker.Replace
        if not self.commonInit():
            return
        self._worker.setMode(Worker.Replace)
        dst = unicode(self.txtDst.text())
        self._worker.setDst(dst)

        self.stkResult.setCurrentWidget(self.pageReplace)
        self.lvReplaceTrace.clear()
        
        self._dlg = QProgressDialog(self)
        self._dlg.setLabelText(self.tr("Replacing..."))
        self._dlg.setMinimumDuration(0)
        QObject.connect(self._dlg, SIGNAL("cancelled()"), self._worker.requestCancel)
        
        self._worker.start()
        self._dlg.exec_()

    
    def fillReplaceListView(self):
        for filename,nb in self._matchList:
            filename = os.path.abspath(filename)
            filename = filename[len(self._absoluteDir)+1:]
            ReplaceTreeWidgetItem(self.lvReplaceTrace, filename, str(nb))

        self.lblStatus.setText(
            self.tr("%1 files modified (took %2 ms)")
            .arg(self.lvReplaceTrace.topLevelItemCount())
            .arg(self._chrono.elapsed()) )
