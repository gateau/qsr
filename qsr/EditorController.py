# vi: expandtab tabstop=4
import os
import shutil

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from Highlighter import Highlighter


class EditorController(QObject):
    def __init__(self, editor):
        QObject.__init__(self, editor)
        
        self._editor = editor

        self._path = u""
        self._mtime = None
        self._highlighter = Highlighter(self._editor.document())

    def _loadFile(self):
        content=file(self._path).read()
        try:
            content=unicode(content, "utf8")
        except UnicodeError:
            self._editor.setEnabled(False)
            self._editor.setText("Can't open file with this encoding")
            return
        self._editor.setEnabled(True)
        self._editor.setPlainText(content)

    def open(self, path, lineNumber):
        mtime=os.stat(path).st_mtime
        if path!=self._path or mtime!=self._mtime:
            self._path=path
            self._mtime=mtime
            self._loadFile()

        cursor = self._editor.textCursor()
        cursor.setPosition(0)
        cursor.movePosition(QTextCursor.Down, QTextCursor.MoveAnchor, lineNumber-1)
        self._editor.setTextCursor(cursor)
        self._editor.ensureCursorVisible()
        self._editor.setFocus()

    def setHighlightRE(self, highlightRE):
        self._highlighter.setHighlightRE(highlightRE)

    def save(self):
        txt = self._editor.toPlainText()
        content=unicode(txt).encode("utf8")
        os.rename(self._path, self._path + ".bak")
        file(self._path, "w").write(content)
        shutil.copymode(self._path + ".bak", self._path)
        self._editor.document().setModified(False)


    def clear(self):
        self._path=""
        self._editor.clear()
