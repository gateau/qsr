#!/usr/bin/python
# vi: expandtab tabstop=4
from PyQt4.QtCore import *
from PyQt4.QtGui import *
import sys
from MainWindow import MainWindow

def main():
    app = QApplication(sys.argv)

    window = MainWindow()
    window.show()
    app.exec_()

if __name__ == "__main__":
    main()
