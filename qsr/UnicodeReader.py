# vi: expandtab tabstop=4
class UnicodeReader:
    def __init__(self):
        self._encodingList = ['utf8', 'iso8859-15']

    def setEncodingList(self, lst):
        self._encodingList = lst

    def read(self, binaryData):
        for encoding in self._encodingList:
            try:
                text = unicode(binaryData, encoding)
                return text, encoding
            except UnicodeError:
                pass
