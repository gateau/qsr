# vi: expandtab tabstop=4
import re

from PyQt4.QtCore import *
from PyQt4.QtGui import *

class Highlighter(QSyntaxHighlighter):
    def __init__(self, parent):
        QSyntaxHighlighter.__init__(self, parent)
        self._highlightRE = None
        self._highlightFormat = QTextCharFormat()
        self._highlightFormat.setBackground(Qt.yellow)
        
    def highlightBlock(self, text):
        if not self._highlightRE:
            return

        text = unicode(text)
        match = self._highlightRE.search(text)
        while match:
            index = match.start()
            length = match.end() - index
            self.setFormat(index, length, self._highlightFormat)
            match = self._highlightRE.search(text, index + length)

    def setHighlightRE(self, highlightRE):
        self._highlightRE = highlightRE
