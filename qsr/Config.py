# vi: expandtab tabstop=4
from PyQt4.QtCore import *

class Config(object):
    __defaultEntries = {
        "patterns":     QString("*"),
        "excludedDirs": QString("CVS .svn .bzr .git"),
        "editor":       QString("gvim %f +%n"),
        "baseDir":      QString(""),
    }
    __slots__ = __defaultEntries.keys()

    def __init__(self):
        settings = self._openSettings()
        for key, value in Config.__defaultEntries.items():
            value = settings.value(key, QVariant(value)).toString()
            object.__setattr__(self, key, value)

    def save(self):
        settings = self._openSettings()
        for key in self.__slots__:
            value = object.__getattribute__(self, key)
            settings.setValue(key, QVariant(value))

    def _openSettings(self):
        return QSettings("qsr", "qsr")
