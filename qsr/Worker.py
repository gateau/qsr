# vi: expandtab tabstop=4
import fnmatch
import os
import re
import shutil
import copy

from PyQt4.QtCore import *

from UnicodeReader import UnicodeReader


class Worker(QThread):
    Unknown=0
    Search=1
    Replace=2
    def __init__(self):
        QThread.__init__(self)
        self._src=u""
        self._dst=u""
        self._mode=Worker.Unknown
        self._cancelMutex=QMutex()
        self._cancel=False
        self._patterns=[]
        self._unicodeReader = UnicodeReader()

        self._excludedDirs=[]
        self._matches=[]
        self._progressMutex=QMutex()
        self._progress=(0,0)

    def requestCancel(self):
        self.setCancel(True)

    def setCancel(self, value):
        self._cancelMutex.lock()
        self._cancel=value
        self._cancelMutex.unlock()

    def cancel(self):
        self._cancelMutex.lock()
        cancel=self._cancel
        self._cancelMutex.unlock()
        return cancel

    def setSearchRE(self, searchRE):
        self._src=searchRE

    def searchRE(self):
        return self._src

    def setDst(self,value):
        self._dst=value

    def setDir(self,value):
        self._dir=value

    def setPatterns(self,patterns):
        self._patterns=patterns

    def setMode(self, mode):
        self._mode=mode

    def setExcludedDirs(self, dirs):
        self._excludedDirs=dirs

    def run(self):
        self._matches=[]
        self._setProgress(0, 0)
        try:
            files=self._findFiles()
            if self.cancel(): return

            nb=len(files)
            pos=0
            if self._mode==Worker.Search:
                method=Worker._search
            else:
                method=Worker._replace

            for file in files:
                method(self,file)
                if self.cancel(): return
                pos+=1
                self._setProgress(pos, nb)
        finally:
            self.setCancel(False)
    

    def progress(self):
        self._progressMutex.lock()
        progress=copy.deepcopy(self._progress)
        self._progressMutex.unlock()
        return progress


    def matches(self):
        assert not self.isRunning()
        return self._matches
    
    def src(self):
        return self._src

#- Private ------------------------------------------
    def _setProgress(self, count, total):
        self._progressMutex.lock()
        self._progress=(count, total)
        self._progressMutex.unlock()


    def _findFiles(self):
        # This functions is a non-recursive equivalent of os.path.walk
        def listDir(baseDir):
            files=[]
            dirs=[]
            try:
                names=os.listdir(baseDir)
            except os.error:
                return files, dirs
            for name in names:
                path=os.path.join(baseDir, name)
                try:
                    isDir=os.path.isdir(path)
                except os.error:
                    continue
                
                if isDir:
                    if not name in self._excludedDirs:
                        dirs.append(path)
                else:
                    for pattern in self._patterns:
                        if fnmatch.fnmatch(name, pattern):
                            files.append(path)
                            break

            return files, dirs


        files=[]
        dirs=[self._dir]
        pos=0
        while pos<len(dirs):
            if self.cancel():
                return []
            newFiles, newDirs=listDir(dirs[pos])
            files.extend(newFiles)
            dirs.extend(newDirs)
            pos+=1

        return files


    def _search(self, filename):
        try:
            content = file(filename).read()
            content, encoding = self._unicodeReader.read(content)
        except Exception, exc:
            print "Error reading file %s" % filename
            print exc
            return
        lineNum=1
        for line in content.split("\n"):
            if self._src.search(line)!=None:
                self._matches.append( (filename, lineNum, line.strip()) )
            lineNum+=1


    def _replace(self, filename):
        try:
            content=file(filename).read()
            content, encoding = self._unicodeReader.read(content)
        except Exception, exc:
            print "Error reading file %s" % filename
            print exc
            return
        content, nb=self._src.subn(self._dst, content)
        if nb==0:
            return
        
        newFilename=filename+".qsrnew"
        bakFilename=filename+".bak"
        try:
            content = content.encode(encoding)
            file(newFilename, 'w').write(content)
            shutil.copymode(filename, newFilename)
        except Exception, exc:
            if os.path.exists(newFilename):
                os.unlink(newFilename)
            print "Error writing file %s" % newFilename
            print exc
            self._matches.append( (filename, -1) )
            return
        os.rename(filename, bakFilename)
        os.rename(newFilename, filename)
        
        self._matches.append( (filename, nb) )
